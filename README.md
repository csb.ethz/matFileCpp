Info
=====
matFileCpp is a C++ libary for reading/writing v7.3 HDF5 .mat MATLAB files to/from Eigen [[1]] matrices. It is used in the reaction-diffusion master equation simulation engine [[2]].

License
-------
matFileCpp is provided under the [Mozilla Public License Version 2.0](LICENSE).

Authors
-------

    Lukas Widmer <lukas.widmer@bsse.ethz.ch>


Installation
============

Windows
-------
1. Install MATLAB
2. Install Git via TortoiseGit (https://tortoisegit.org/)
3. Install Mercurial via TortoiseHg (https://tortoisehg.bitbucket.io/)
4. Install a C++ Compiler, e.g., Visual Studio (https://www.visualstudio.com/). If you do not run "Visual Studio 14 2015 Win64", adjust this setting in [Example/compileTest.m](Example/compileTest.m)
5. Install CMake (https://cmake.org/)
6. Install HDF5 binaries for your C++ compiler (https://support.hdfgroup.org/HDF5/)
7. Git clone this repo

Mac OS X
--------
1. Install MATLAB
2. Install Git through package manager: e.g., `brew install git`
3. Install Mercurial through package manager: e.g., `brew install mercurial`
4. Install a C++ Compiler, e.g., XCode (https://developer.apple.com/xcode/)
5. Install CMake (https://cmake.org/)
6. Install HDF5 through package manager: e.g., `brew install hdf5`
7. Git clone this repo

Linux (instructions here are for Ubuntu / Debian)
-------------------------------------------------
1. Install MATLAB
2. Install Git through package manager: `sudo apt-get install git`
3. Install Mercurial through package manager: `sudo apt-get install mercurial`
4. Install a C++ Compiler, e.g., g++: `sudo apt-get install g++`
5. Install CMake: `sudo apt-get install cmake`
6. Install HDF5: `apt-get install libhdf5-10 libhdf5-cpp-11 libhdf5-dev`
7. Git clone this repo

Cluster Usage
=============
Git and Mercurial should be installed by default.
1. Load MATLAB module: `module load matlab`
2. Load HDF5: `module load hdf5`
3. Load C++ compiler: `module load gcc/5.3.0`
4. Load CMake: `module load cmake/3.3.1`
5. Git clone this repo

Usage 
=====
Run [Example/runTest.m](Example/runTest.m) in MATLAB




References
==========
[1]: http://eigen.tuxfamily.org
1. [Eigen Library][1]
[2]: http://google.com
2. Widmer LA, Stelling J (2017)  
RDMEcpp: a cross-platform, modular, and fast C++ reaction-diffusion master equation simulator  
[Submitted.][2]