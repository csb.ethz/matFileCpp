/*
This file is part of matFileCpp.
© 2017, ETH Zurich, Lukas Widmer (l.widmer@gmail.com)

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this file,
You can obtain one at https://mozilla.org/MPL/2.0/
*/

#pragma once
#include <string>
#include <functional>
#include <vector>
#include <Eigen/Dense>
#include <Eigen/SparseCore>
#include <iostream>
#include <iomanip>
#include <stdexcept>
#ifdef _WIN32
#define H5_BUILT_AS_DYNAMIC_LIB
#endif

#include <H5Cpp.h>

class MatlabFile
{
public:
	static MatlabFile Open(const std::string& path, const std::string& rootGroup)
	{
		return MatlabFile(path, rootGroup, H5F_ACC_RDONLY);
	}

	static MatlabFile Create(const std::string& path, const std::string& rootGroup)
	{
		return MatlabFile(path, rootGroup, H5F_ACC_TRUNC);
	}

	MatlabFile(const std::string &path, const std::string &rootGroup, const decltype(H5F_ACC_RDONLY) mode)
	{
		path_ = path;
		mode_ = mode;

		if (mode == H5F_ACC_RDONLY)
		{
			h5File_ = H5::H5File(path.c_str(), mode);
			rootGroup_ = h5File_.openGroup(rootGroup.c_str());
		}
		else if (mode == H5F_ACC_TRUNC)
		{
			auto foo = H5::FileCreatPropList();
			foo.setUserblock(512);
			h5File_ = H5::H5File(path.c_str(), mode, foo);

			if (rootGroup == "/")
				rootGroup_ = h5File_.openGroup("/");
			else
				rootGroup_ = h5File_.createGroup(rootGroup.c_str());

			SetStringAttribute(rootGroup_, "MATLAB_class", "struct");
		}
		else
		{
			throw std::runtime_error("Unknown MatlabFile Mode!");
		}
	}

	~MatlabFile(void)
	{
		if (mode_ == H5F_ACC_TRUNC)
		{
			h5File_.close();

			char header[512] = "MATLAB 7.3 MAT-file, created by MatFileCpp";
			header[124] = 0;
			header[125] = 2;
			header[126] = 'I';
			header[127] = 'M'; // Magic MATLAB signature

			FILE* stream;
#ifdef WIN32
			stream = _fsopen(path_.c_str(), "r+b", _SH_DENYNO);
#else
			stream = fopen(path_.c_str(), "r+b");
#endif
			fwrite(header, 1, 512, stream);
			fclose(stream);
		}
	}

	Eigen::SparseMatrix<double> GetSparseMatrix(const std::string& var) const
	{
		H5::Group sparseMatrixGroup = rootGroup_.openGroup(var.c_str());

		H5::DataSet dataSet = sparseMatrixGroup.openDataSet("data");
		H5::DataSpace dataSpace = dataSet.getSpace();

		int nDimensions = dataSpace.getSimpleExtentNdims();
		// std::cout << "Dimensions: " << nDimensions << std::endl;

		if (nDimensions != 1)
		{
			throw std::runtime_error("Sparse matrix data must have 1 dimension!");
		}

		hssize_t nRows = 0;
		if (sparseMatrixGroup.attrExists("MATLAB_sparse"))
		{
			H5::Attribute nRowsAttribute = sparseMatrixGroup.openAttribute("MATLAB_sparse");
			// TODO: Check Type Compatibility
			nRowsAttribute.read(H5::PredType::NATIVE_INT64, &nRows);
			nRowsAttribute.close();
		}
		else
		{
			throw std::runtime_error("Matrix row dimension missing (MATLAB_sparse attribute)!");
		}

		hssize_t nPoints = dataSpace.getSimpleExtentNpoints();
		// std::cout << "nPoints: " << nPoints << std::endl;

		if (nPoints == 0)
		{
			return Eigen::SparseMatrix<double>();
		}

		H5::DataSet rowIndexDataSet = sparseMatrixGroup.openDataSet("ir");
		H5::DataSpace rowIndexDataSpace = rowIndexDataSet.getSpace();
		int nIrDims = rowIndexDataSpace.getSimpleExtentNdims();

		if (nIrDims != 1)
		{
			throw std::runtime_error("Sparse matrix row indices must have 1 dimension!");
		}

		hssize_t nRowPoints = rowIndexDataSpace.getSimpleExtentNpoints();

		if (nPoints != nRowPoints)
		{
			throw std::runtime_error("Each sparse matrix entry must have exactly row index!");
		}

		H5::DataSet columnStartsDataSet = sparseMatrixGroup.openDataSet("jc");
		H5::DataSpace columnStartsDataSpace = columnStartsDataSet.getSpace();
		int nColumnStartDimensions = columnStartsDataSpace.getSimpleExtentNdims();

		if (nColumnStartDimensions != 1)
		{
			throw std::runtime_error("Sparse matrix column indices must have 1 dimension!");
		}

		hssize_t nColumnStartPoints = columnStartsDataSpace.getSimpleExtentNpoints();

		if (nColumnStartPoints < 1)
		{
			throw std::runtime_error("At least one sparse matrix column index start must be defined!");
		}

		hssize_t nColumns = nColumnStartPoints - 1;

		// TO DO: Check Native Type Compatibility!
		double* matrixEntries = new double[nPoints];
		dataSet.read(matrixEntries, H5::PredType::NATIVE_DOUBLE);

		hssize_t* matrixRowIndices = new hssize_t[nRowPoints];
		rowIndexDataSet.read(matrixRowIndices, H5::PredType::NATIVE_INT64);

		hssize_t* matrixColumnStarts = new hssize_t[nColumnStartPoints];
		columnStartsDataSet.read(matrixColumnStarts, H5::PredType::NATIVE_INT64);

		std::vector<Eigen::Triplet<double>> triplets;
		triplets.reserve(nPoints);

		hssize_t iStart = 0;
		hssize_t iEnd = 0;
		for (hssize_t j = 0; j < nColumnStartPoints - 1; j++)
		{
			iStart = matrixColumnStarts[j];
			iEnd = matrixColumnStarts[j + 1];
			for (hssize_t i = iStart; i < iEnd; i++)
			{
				triplets.push_back(Eigen::Triplet<double>(matrixRowIndices[i], j, matrixEntries[i]));
			}
		}

		// std::cout << "Total Rows: " << nRows << std::endl;
		// std::cout << "Total Cols: " << nColumns << std::endl;

		delete[] matrixEntries;
		delete[] matrixRowIndices;
		delete[] matrixColumnStarts;

		Eigen::SparseMatrix<double> mat(nRows, nColumns);
		mat.setFromTriplets(triplets.begin(), triplets.end());
		mat.makeCompressed();

		return mat;
	}

	Eigen::MatrixXd GetMatrix(const std::string& var) const
	{
		H5::DataSet dataset = rootGroup_.openDataSet(var.c_str());
		if (!H5Tequal(dataset.getDataType().getId(), H5T_NATIVE_DOUBLE))
		{
			if (dataset.attrExists("MATLAB_empty"))
			{
				return Eigen::MatrixXd();
			}
			else
			{
				throw std::runtime_error("Matrix of double values expected!");
			}
		}

		H5::DataSpace dataSpace = dataset.getSpace();

		const int nDimensions = dataSpace.getSimpleExtentNdims();

		if (nDimensions != 2)
		{
			throw std::runtime_error("Matrix must have 2 dimensions!");
		}

		hsize_t dimensions[2];
		dataSpace.getSimpleExtentDims(dimensions);


		/* 
		std::cout << dataSpace.getSimpleExtentNpoints() << std::endl;
		for (hsize_t i = 0; i < nDimensions; i++)
		{
			std::cout << "Dim " << i << ": " << dimensions[i] << std::endl;
		}
		*/

		double* matrixEntries = new double[dimensions[0] * dimensions[1]];

		dataset.read(matrixEntries, H5::PredType::NATIVE_DOUBLE);
		dataset.close();

		Eigen::MatrixXd retVal = Eigen::Map<Eigen::MatrixXd>(matrixEntries, dimensions[1], dimensions[0]);

		delete[] matrixEntries;

		return retVal;
	}

	Eigen::VectorXd GetVector(const std::string& var) const
	{
		return GetMatrix(var);
	}

	Eigen::RowVectorXd GetRowVector(const std::string& var) const
	{
		return GetMatrix(var);
	}

	double GetValue(const std::string& var) const
	{
		Eigen::MatrixXd temp = GetMatrix(var);
		if (temp.rows() != 1 || temp.cols() != 1)
		{
			throw std::runtime_error("Value must be a 1x1 Matrix!");
		}
		return temp.value();
	}

	void SetSparseMatrix(const std::string& var, const Eigen::SparseMatrix<double>& matrix)
	{
		auto matrixGroup = rootGroup_.createGroup(var.c_str());
		SetStringAttribute(matrixGroup, "MATLAB_class", "double");
		SetDoubleAttribute(matrixGroup, "MATLAB_sparse", matrix.rows());

		std::vector<hsize_t> dimensions(1); dimensions[0] = matrix.nonZeros();
		WriteDoubleDataset(matrixGroup, "data", dimensions, matrix.valuePtr());

		WriteUIntDataset(matrixGroup, "ir", dimensions, matrix.innerIndexPtr());

		std::vector<hsize_t> colDimensions(1); colDimensions[0] = matrix.outerSize() + 1;
		WriteUIntDataset(matrixGroup, "jc", colDimensions, matrix.outerIndexPtr());
	}

	void SetMatrix(const std::string& var, const Eigen::MatrixXd& matrix)
	{
		std::vector<hsize_t> dimensions(2);
		dimensions[0] = matrix.cols();
		dimensions[1] = matrix.rows();

		H5::DataSet dataset = WriteDoubleDataset(rootGroup_, var, dimensions, matrix.data());

		SetStringAttribute(dataset, "MATLAB_class", "double");
	}

	void SetVector(const std::string& var, const Eigen::VectorXd& matrix)
	{
		SetMatrix(var, matrix);
	}

	void SetRowVector(const std::string& var, const Eigen::RowVectorXd& matrix)
	{
		SetMatrix(var, matrix);
	}

	void SetValue(const std::string& var, const double value)
	{
		auto temp = Eigen::VectorXd(1);
		temp(0) = value;
		SetVector(var, temp);
	}

	void CreateGroup(const std::string& path)
	{
		h5File_.createGroup(path.c_str());
	}
protected:
	static void SetStringAttribute(const H5::H5Object& location, const std::string& var, const std::string& value)
	{
		auto type = H5::StrType(H5::PredType::C_S1);
		type.setSize(value.length());
		auto attr = location.createAttribute(var.c_str(), type, H5::DataSpace());
		attr.write(type, value.c_str());
	}

	static void SetDoubleAttribute(const H5::H5Object& location, const std::string& var, const double value)
	{
		auto type = H5::FloatType(H5::PredType::NATIVE_DOUBLE);
		auto attr = location.createAttribute(var.c_str(), type, H5::DataSpace());
		attr.write(type, &value);
	}

	static H5::DataSet WriteDoubleDataset(const H5::Group& location, const std::string& name, const std::vector<hsize_t>& dimensions, const void* buffer)
	{
		auto dataType = H5::FloatType(H5::PredType::NATIVE_DOUBLE);
		dataType.setOrder(H5T_ORDER_LE);

		return WriteDataset(location, name, dataType, dimensions, buffer);
	}

	static H5::DataSet WriteUIntDataset(const H5::Group& location, const std::string& name, const std::vector<hsize_t>& dimensions, const void* buffer)
	{
		auto dataType = H5::IntType(H5::PredType::NATIVE_INT32);
		dataType.setOrder(H5T_ORDER_LE);

		return WriteDataset(location, name, dataType, dimensions, buffer);
	}
	static H5::DataSet WriteDataset(const H5::Group& location, const std::string& name, const H5::DataType& dataType, const std::vector<hsize_t>& dimensions, const void* buffer)
	{
		H5::DataSpace dataSpace(dimensions.size(), dimensions.data());

		H5::DataSet dataset = location.createDataSet(name.c_str(), dataType, dataSpace);
		dataset.write(buffer, dataType);

		return dataset;
	}

	std::string path_;
	decltype(H5F_ACC_RDONLY) mode_;
	H5::Group rootGroup_;
	H5::H5File h5File_;
};

