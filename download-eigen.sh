#!/bin/bash
# This file is part of matFileCpp.
# © 2017, ETH Zurich, Lukas Widmer (l.widmer@gmail.com)

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this file,
# You can obtain one at https://mozilla.org/MPL/2.0/
mv Eigen/.gitignore .gitignore.Eigen
hg clone -u 3.3.2 https://bitbucket.org/eigen/eigen Eigen
mv .gitignore.Eigen Eigen/.gitignore