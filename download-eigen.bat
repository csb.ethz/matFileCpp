@REM This file is part of matFileCpp.
@REM © 2017, ETH Zurich, Lukas Widmer (l.widmer@gmail.com)

@REM This Source Code Form is subject to the terms of the Mozilla Public
@REM License, v. 2.0. If a copy of the MPL was not distributed with this file,
@REM You can obtain one at https://mozilla.org/MPL/2.0/
move Eigen\.gitignore .gitignore.Eigen
hg clone -u 3.3.2 https://bitbucket.org/eigen/eigen Eigen
move .gitignore.Eigen Eigen\.gitignore