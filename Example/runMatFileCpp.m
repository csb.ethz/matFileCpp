% This file is part of matFileCpp.
% © 2017, ETH Zurich, Lukas Widmer (l.widmer@gmail.com)

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this file,
% You can obtain one at https://mozilla.org/MPL/2.0/

function runMatFileCpp(isCompiledAsDebug)
    if nargin == 0
        isCompiledAsDebug = false;
    end
    if isCompiledAsDebug
        compileConfig = 'Debug';
    else
        compileConfig = 'Release';
    end
    if ispc
        currentPath = getenv('PATH');
        cmd = ['PATH=' currentPath ' && bin\' compileConfig '\example.exe'];
    else
        cmd = ['LD_LIBRARY_PATH= && bin/example'];
    end
    system(cmd)
end