% This file is part of matFileCpp.
% © 2017, ETH Zurich, Lukas Widmer (l.widmer@gmail.com)

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this file,
% You can obtain one at https://mozilla.org/MPL/2.0/

clear all;
clc;

%%
if ~exist(['..' filesep 'Eigen' filesep 'Eigen'], 'dir')
    warning('Eigen must be downloaded before attempting to run the tests. Attempting to download it. Note that this requires installation of mercurial/hg on Linux/OS X and TortoiseHg on Windows.');
    
    cd('..');
    if ispc
        status = system('download-eigen.bat')
    else
        if system('which hg') ~= 0
            cd('Example');
            error('Please install Mercurial/hg!');
        end
        status = system('LD_LIBRARY_PATH= && ./download-eigen.sh')
    end
    cd('Example');
    if status ~= 0
        error('Could not download Eigen');
    end
end

%% Compile
compileTest

%% Create test vectors

colVector = (1:3)';
rowVector = 1:3;
matrix = zeros(3);
matrix(:) = 1:9;
scalar = 42;
sparseMatrix = sparse(matrix);
sparseMatrix([1 5 9]) = 0;


%colVector
%rowVector
%matrix
%scalar
%full(sparseMatrix)
%% Save test vectors
save('testInput.mat', '-v7.3');

%% Run executable
runMatFileCpp
%%
result = load('testOutput.mat');

fprintf('colVector    read/write test: %d\n', double(all(result.colVector == colVector)));
fprintf('rowVector    read/write test: %d\n', double(all(result.rowVector == rowVector)));
fprintf('matrix       read/write test: %d\n', double(all(all(result.matrix == matrix))));
fprintf('scalar       read/write test: %d\n', double(result.scalar == scalar));
fprintf('sparseMatrix read/write test: %d\n', full(all(all(result.sparseMatrix == sparseMatrix))));