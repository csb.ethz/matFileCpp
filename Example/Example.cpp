#include <matFileCpp/MatlabFile.hpp>


int main()
{
	auto testInput = MatlabFile::Open("testInput.mat", "/");

	auto colVector = testInput.GetVector("colVector");
	std::cout << colVector;
	auto rowVector = testInput.GetRowVector("rowVector");
	std::cout << rowVector;
	auto matrix = testInput.GetMatrix("matrix");
	std::cout << matrix;
	auto scalar = testInput.GetValue("scalar");
	std::cout << scalar;
	auto sparseMatrix = testInput.GetSparseMatrix("sparseMatrix");
	std::cout << sparseMatrix;

	Eigen::SparseMatrix<double>::InnerIterator it(sparseMatrix, 1);
	for (; it; ++it)
	{
		std::cout << "Col: " << it.col() << ", Row: " << it.row() << ", Value: " << it.value() << std::endl;
	}
	
	auto testOutput = MatlabFile::Create("testOutput.mat", "/");
	testOutput.SetVector("colVector", colVector);
	testOutput.SetRowVector("rowVector", rowVector);
	testOutput.SetMatrix("matrix", matrix);
	testOutput.SetValue("scalar", scalar);
	testOutput.SetSparseMatrix("sparseMatrix", sparseMatrix);

	// End on a nice and clean zero return value
	return 0;
}