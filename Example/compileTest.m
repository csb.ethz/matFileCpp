% This file is part of matFileCpp.
% © 2017, ETH Zurich, Lukas Widmer (l.widmer@gmail.com)

% This Source Code Form is subject to the terms of the Mozilla Public
% License, v. 2.0. If a copy of the MPL was not distributed with this file,
% You can obtain one at https://mozilla.org/MPL/2.0/
function compileTest(compileAsDebug)
    if exist('bin/','dir') > 0
        rmdir('bin/', 's');
    end

    if nargin == 0
        compileAsDebug = false;
    end
    
    if compileAsDebug
        compileConfig = 'Debug';
    else
        compileConfig = 'Release';
    end
        
    mkdir('bin');
    
    if ispc
        cmd = ['cmake -G "Visual Studio 14 2015 Win64" ".." && cmake --build . --target ALL_BUILD --config ' compileConfig];
    else
        cmd = ['LD_LIBRARY_PATH= && cmake ".." && cmake --build . --config ' compileConfig];
    end

    currentPath = cd('bin');
    status = system(cmd)
    cd(currentPath);
    
    if status ~= 0
        error('Compilation failed: Error %i\n',status);
    end
end